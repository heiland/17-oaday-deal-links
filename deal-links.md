MPI OA day
---

 * [the deal project](https://www.projekt-deal.de/aktuelles/)
 * [2017.10 -- scinoptica -- Springer/Elsevier](https://www.scinoptica.com/2017/10/news-on-deal-springer-nature-elsevier/)
 * [2017.08 -- boersenblatt -- no progress](https://www.boersenblatt.net/artikel-kein_deal_mit_elsevier.1306764.html)
 * [2017.02 -- nature -- regain access](http://www.nature.com/news/german-scientists-regain-access-to-elsevier-journals-1.21482?utm_content=buffer2bea8&utm_medium=social&utm_source=twitter.com&utm_campaign=buffer)
 * [2016.12 -- nature -- cut off](http://www.nature.com/news/scientists-in-germany-peru-and-taiwan-to-lose-access-to-elsevier-journals-1.21223)
 * [many ways to get access](http://bjoern.brembs.net/2016/12/so-your-institute-went-cold-turkey-on-publisher-x-what-now/)
 * [open access button](https://openaccessbutton.org/) `<< https://doi.org/10.1051/m2an/2015029`
 * conclusion: ask your librarian
